package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

public class Cercle implements FiguraGeometrica{
    private final double radi;
    private final double pi=Math.PI;
    
    public Cercle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del radi: ");
        this.radi = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return pi*radi*radi;
    }
    
    @Override
    public double calcularPerimetre() {
        return 2*pi*radi;
    }
}
