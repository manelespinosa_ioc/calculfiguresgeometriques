package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

public class Rectangle implements FiguraGeometrica{
    private final double costat1;
    private final double costat2;
    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del costat 1 del rectange: ");
        this.costat1 = scanner.nextDouble();
        System.out.println("Introduïu la longitud del costat 2 del rectange: ");
        this.costat2 = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return costat1 * costat2;
    }
    
    @Override
    public double calcularPerimetre() {
        return (2 * costat1) +(2*costat2);
    }
}
